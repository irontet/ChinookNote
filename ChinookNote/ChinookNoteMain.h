/***************************************************************
 * Name:      ChinookNoteApp.cpp
 * Purpose:   Code for Application Class
 * Author:    Ben Milliron (ben@sapadian.com)
 * Created:   2015-10-27
 * Copyright: Ben Milliron (www.ChinookNote.com)
 * License: GPLv3
 **************************************************************/


#ifndef CHINOOKNOTEMAIN_H
#define CHINOOKNOTEMAIN_H



#include "ChinookNoteApp.h"


#include "GUIFrame.h"

class ChinookNoteFrame: public GUIFrame
{
    public:
        ChinookNoteFrame(wxFrame *frame);
        ~ChinookNoteFrame();
    private:
        virtual void OnClose(wxCloseEvent& event);
        virtual void OnQuit(wxCommandEvent& event);
        virtual void OnAbout(wxCommandEvent& event);
};

#endif // CHINOOKNOTEMAIN_H
