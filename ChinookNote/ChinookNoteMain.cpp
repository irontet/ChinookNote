/***************************************************************
 * Name:      ChinookNoteApp.cpp
 * Purpose:   Code for Application Class
 * Author:    Ben Milliron (ben@sapadian.com)
 * Created:   2015-10-27
 * Copyright: Ben Milliron (www.ChinookNote.com)
 * License: GPLv3
 **************************************************************/


#ifdef WX_PRECOMP
#include "wx_pch.h"
#endif

#ifdef __BORLANDC__
#pragma hdrstop
#endif //__BORLANDC__

#include "ChinookNoteMain.h"
#include "Communicate.h"
#include <wx/panel.h>
#include <wx/window.h>
#include <wx/vlbox.h>

//helper functions
enum wxbuildinfoformat {
    short_f, long_f };

wxString wxbuildinfo(wxbuildinfoformat format)
{
    wxString wxbuild(wxVERSION_STRING);

    if (format == long_f )
    {
#if defined(__WXMSW__)
        wxbuild << _T("-Windows");
#elif defined(__WXMAC__)
        wxbuild << _T("-Mac");
#elif defined(__UNIX__)
        wxbuild << _T("-Linux");
#endif

#if wxUSE_UNICODE
        wxbuild << _T("-Unicode build");
#else
        wxbuild << _T("-ANSI build");
#endif // wxUSE_UNICODE
    }

    return wxbuild;
}


ChinookNoteFrame::ChinookNoteFrame(wxFrame *frame)
    : GUIFrame(frame)
{
#if wxUSE_STATUSBAR
    statusBar->SetStatusText(_("Welcome to ChinookNote!"), 0);
    //statusBar->SetStatusText(wxbuildinfo(short_f), 1);
#endif

int id_but1 = 1, id_but2 = 2;

wxPanel *Panel1 = new wxPanel(this, -1);
    Panel1->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));

    wxButton *button = new wxButton(Panel1, id_but1, "New Note");
    wxButton *button2 = new wxButton(Panel1, id_but2, "New Notebook");


    wxSizer *sizer = new wxBoxSizer(wxVERTICAL);
    Panel1->SetSizer(sizer);

    sizer->Add(button, 1, wxALIGN_LEFT);
    sizer->Add(button2, 1, wxALIGN_LEFT);


    Panel1->SetSizer(sizer);
    sizer->SetSizeHints(this);

    //wxVListBox *vListNoteBooks = new wxVListBox(wxWindow *parent, wxWindowID id=wxID_ANY, const wxPoint &pos=wxDefaultPosition, const wxSize &size=wxDefaultSize, long style=0, const wxString &name=wxVListBoxNameStr);



}


ChinookNoteFrame::~ChinookNoteFrame()
{
}

void ChinookNoteFrame::OnClose(wxCloseEvent &event)
{
    Destroy();
}

void ChinookNoteFrame::OnQuit(wxCommandEvent &event)
{
    Destroy();
}

void ChinookNoteFrame::OnAbout(wxCommandEvent &event)
{
    wxString msg = "Something will go here eventually."; //wxbuildinfo(long_f);
    wxMessageBox(msg, _("About ChinookNote"));

}


