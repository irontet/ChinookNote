/***************************************************************
 * Name:      ChinookNoteApp.cpp
 * Purpose:   Code for Application Class
 * Author:    Ben Milliron (ben@sapadian.com)
 * Created:   2015-10-27
 * Copyright: Ben Milliron (www.ChinookNote.com)
 * License: GPLv3
 **************************************************************/

#ifdef WX_PRECOMP
#include "wx_pch.h"
#endif

#ifdef __BORLANDC__
#pragma hdrstop
#endif //__BORLANDC__

#include "ChinookNoteApp.h"
#include "ChinookNoteMain.h"
#include "Button.h"
#include "Communicate.h"

IMPLEMENT_APP(ChinookNoteApp);

bool ChinookNoteApp::OnInit()
{
    ChinookNoteFrame* frame = new ChinookNoteFrame(0L);


    frame->Show();



    return true;
}
