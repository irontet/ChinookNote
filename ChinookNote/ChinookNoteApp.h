/***************************************************************
 * Name:      ChinookNoteApp.cpp
 * Purpose:   Code for Application Class
 * Author:    Ben Milliron (ben@sapadian.com)
 * Created:   2015-10-27
 * Copyright: Ben Milliron (www.ChinookNote.com)
 * License: GPLv3
 **************************************************************/

#ifndef CHINOOKNOTEAPP_H
#define CHINOOKNOTEAPP_H

#include <wx/app.h>

class ChinookNoteApp : public wxApp
{
    public:
        virtual bool OnInit();
};

#endif // CHINOOKNOTEAPP_H
